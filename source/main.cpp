#include "MicroBit.h"

//#define DEBUG 1

MicroBit uBit;
char command;
const uint8_t groupBase = 150;
volatile uint8_t group = 0;

void sendCommand(char s, int pitch = 0, int roll = 0)
{
    if (pitch || roll || !(s == command)) {
        command = s;
        uBit.display.printChar(command);
        // we need to convert the pitch and roll to 0 - 255
        // where 0° = 127, -180° = 0, +180° = 255
        uint8_t data[3] = {s, (uint8_t) ((pitch + 180) * 0.71), (uint8_t) ((roll + 180) * 0.71)};
        uBit.radio.datagram.send(data, 3);
#ifdef DEBUG
        uBit.serial.printf("Data Send: %c, %d, %d\r\n", s, pitch, roll);
#endif
    }
}

int main()
{
    register int pitch, roll;
    // Initialise the micro:bit runtime.
    uBit.init();

    // Debug output
#ifdef DEBUG
    uBit.serial.baud(57600);
#endif

    // Radio
    uBit.radio.setGroup(groupBase + group);
    uBit.radio.enable();

    // set up pins
    uBit.io.P8.setPull(PullUp);
    uBit.io.P12.setPull(PullUp);
    uBit.io.P13.setPull(PullUp);
    uBit.io.P14.setPull(PullUp);
    uBit.io.P15.setPull(PullUp);
    uBit.io.P16.setPull(PullUp);

    // main loop
    while(true) {
        // switch radio group
        if (uBit.buttonAB.isPressed()) {
            group = 1 - group;
            uBit.radio.disable();
            uBit.radio.setGroup(groupBase + group);
            uBit.radio.enable();
            uBit.display.printChar('0' + group);
            uBit.sleep(1000);
        }
        // controll with rotation and pitch
        while (uBit.buttonA.isPressed()) {
            uBit.display.clear();
            pitch = uBit.accelerometer.getPitch();
            roll = uBit.accelerometer.getRoll();
            sendCommand('x', pitch, roll);
            uBit.sleep(50);
        }
        // forward
        while (uBit.io.P8.getDigitalValue() == 0) {
            sendCommand('f');
        }
        // back
        while (uBit.io.P14.getDigitalValue() == 0) {
            sendCommand('b');
        }
        // left
        while (uBit.io.P12.getDigitalValue() == 0) {
            sendCommand('l');
        }
        // right
        while (uBit.io.P13.getDigitalValue() == 0) {
            sendCommand('r');
        }
        // cipper
        while (uBit.io.P15.getDigitalValue() == 0) {
            sendCommand('c');
        }
        // enable line following
        while (uBit.io.P16.getDigitalValue() == 0) {
            sendCommand('a');
        }
        if (!(command == 'a')) {
            sendCommand('s');
        }
        uBit.sleep(50);
    }

    release_fiber();
    return 0;
}
